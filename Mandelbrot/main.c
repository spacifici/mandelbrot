//
//  main.c
//  Mandelbrot
//
//  Created by Stefano Pacifici on 28.04.19.
//  Copyright © 2019 Stefano Pacifici. All rights reserved.
//

#include <stdio.h>
#include <SDL2/SDL.h>
#include <gmp.h>

struct _mapper {
    mpq_t m;
    mpq_t q;
};
typedef struct _mapper mapper_t;

struct _complex {
    mpq_t r;
    mpq_t i;
};
typedef struct _complex complex_t;

SDL_Window *win = NULL;
SDL_Renderer *ren = NULL;

int width = 600;
int height = 600;
double minX = -2.5;
double maxX = 1.5;
double minY = -2.0;
double maxY = 2.0;
double zoom = 2.0;
int iterations = 100;

void mapperInit(mapper_t* outMapper,
                const mpq_t minD, const mpq_t maxD,
                const mpq_t minC, const mpq_t maxC) {
    // minC = m * minD + q
    // maxC = m * maxD + q
    // m = (maxC - minC) / (maxD - minD)
    // q = maxC - maxD * m
    mpq_t dC, dD, mxd;
    mpq_inits(outMapper->m, outMapper->q, dC, dD, mxd, NULL);
    mpq_sub(dC, maxC, minC);
    mpq_sub(dD, maxD, minD);
    mpq_div(outMapper->m, dC, dD);
    mpq_mul(mxd, maxD, outMapper->q);
    mpq_sub(outMapper->q, maxC, mxd);
    mpq_clears(dC, dD, mxd);
}

void mapperClear(mapper_t * outMapper) {
    mpq_clears(outMapper->m, outMapper->q);
}

void mapperApply(mpq_t outValue, const mpq_t inValue, const mapper_t* mapper) {
    mpq_t mxiv;
    mpq_inits(mxiv, NULL);
    mpq_mul(mxiv, mapper->m, inValue);
    mpq_add(outValue, mxiv, mapper->q);
    mpq_clears(mxiv, NULL);
}

void complexIterate(complex_t* inOut, const complex_t* base) {
    // a^2 - b^2 + i2ab
    mpq_t aa, bb, ab2;
    mpq_inits(aa, bb, ab2, NULL);
    mpq_mul(aa, inOut->r, inOut->r);
    mpq_mul(bb, inOut->i, inOut->i);
    mpq_set_si(ab2, 2, 1);
    mpq_mul(ab2, ab2, inOut->r);
    mpq_mul(ab2, ab2, inOut->i);
    
    // r = an^2 - bn^2 + a0
    mpq_sub(inOut->r, aa, bb);
    mpq_add(inOut->r, inOut->r, base->r);

    // i = 2anbn + b0
    mpq_add(inOut->i, ab2, base->i);
    
    mpq_clears(aa, bb, ab2, NULL);
}

double map(double v, double minD, double maxD, double minC, double maxC) {
    double m = (maxC - minC) / (maxD - minD);
    double q = minC - m * minD;
    return m * v + q;
}

void hsvToRgb(float h, float s, float v, Uint8 *r, Uint8 *g, Uint8 *b) {
    float C = v * s;
    float X = C * (1 - abs( ((int)(h * 6.0f))%2 - 1));
    float m = v - C;
    int nh = (int) (h * 6.0f);
    float R = 0.0f, G = 0.0f, B = 0.0f;
    switch (nh) {
        case 0:
            R = C;
            G = X;
            B = 0;
            break;
        case 1:
            R = X;
            G = C;
            B = 0;
            break;
        case 2:
            R = 0;
            G = C;
            B = X;
            break;
        case 3:
            R = 0;
            G = X;
            B = C;
            break;
        case 4:
            R = X;
            G = 0;
            B = C;
            break;
        case 5:
            R = C;
            G = 0;
            B = X;
            break;
    }
    *r = (Uint8) ((R + m) * 255.0f);
    *g = (Uint8) ((G + m) * 255.0f);
    *b = (Uint8) ((B + m) * 255.0f);
}

void setup() {
    if (SDL_Init(SDL_INIT_VIDEO) != 0) {
        fprintf(stderr, "Error initializing SDL2\n");
        exit(-1);
    }
    win = SDL_CreateWindow("Mandelbrot",
                           0, 0, width, height,
                           SDL_WINDOW_SHOWN);
    if (win == NULL) {
        fprintf(stderr, "Can't create a Window");
        exit(-1);
    }
    
    ren = SDL_CreateRenderer(win, 0, SDL_RENDERER_ACCELERATED);
    if (ren == NULL) {
        fprintf(stderr, "Can't create a renderer");
        exit(-1);
    }
}

void draw() {
    for(int y = 0; y < height; y++) {
        for (int x = 0; x < width; x++) {
            double a = map(x, 0, width, minX, maxX);
            double b = map(y, 0, height, maxY, minY);
            double aa = 0;
            double bb = 0;
            
            int n = 0;
            while (n < iterations) {
                double as = aa * aa;
                double bs = bb * bb;
                if (fabs(as+bs) > 4) {
                    break;
                }
                // (a+bi)*(a+bi) = a^2 + 2abi - b^2
                double aap = as - bs;
                double bbp = 2 * aa * bb;
                
                aa = aap + a;
                bb = bbp + b;
                
                n++;
            }
            
            float H = ((float) n) / ((float) iterations);
            float V = (float) (n != iterations);
            Uint8 R, G, B;
            hsvToRgb(H, 1.0f, V, &R, &G, & B);
            
            SDL_SetRenderDrawColor(ren, R, G, B, 255);
            SDL_RenderDrawPoint(ren, x, y);
        }
    }
    SDL_RenderPresent(ren);
}

void loop() {
    SDL_Event e;
    int quit = 0;
    while (!quit) {
        SDL_WaitEvent(&e);
        switch(e.type) {
            case SDL_QUIT:
                quit = 1;
                break;
//            case SDL_MOUSEBUTTONUP:
//                if (e.button.button == SDL_BUTTON_LEFT) {
//                    double x = map(e.button.x, 0, width, minX, maxX);
//                    double y = map(e.button.y, 0, height, maxY, minY);
//                    zoom /= 2.0;
//                    minX = x - zoom;
//                    maxX = x + zoom;
//                    minY = y - zoom;
//                    maxY = y + zoom;
//                    draw();
//                }
//                break;
            default:
                break;
        }
    }
}

void destroy() {
    SDL_DestroyRenderer(ren);
    SDL_DestroyWindow(win);
}

int main(int argc, const char * argv[]) {
    setup();
    draw();
    loop();
    destroy();
    return 0;
}
